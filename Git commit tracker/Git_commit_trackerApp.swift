//
//  Git_commit_trackerApp.swift
//  Git commit tracker
//
//  Created by Julie PO on 2020/10/08.
//

import SwiftUI

@main
struct Git_commit_trackerApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
