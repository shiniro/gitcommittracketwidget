//
//  ContentView.swift
//  Git commit tracker
//
//  Created by Julie PO on 2020/10/08.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, world!")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
